# from enum import StrEnum -- does not work???
import pickle

def load_mu(filename):
    result = {}
    with open(filename, 'rb') as f:
        result = pickle.load(f)
    return result

def save_mu(filename, data):
    with open(filename, 'wb') as f:
        pickle.dump(data, f)

class AlbumEntry():
    def __init__(self, name, year, status):
        self.name = name
        self.year = year
        self.status = status

    def __str__(self):
        return f"{self.year} - {self.name} [{self.status}]"
