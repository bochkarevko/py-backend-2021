from typing import Optional

from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

import mu_list

app = FastAPI()

mu_dict = mu_list.load_mu('data.pkl')
# mu_list.save_mu('data.pkl', new_data)

class NewAlbumRequest(BaseModel):
    artist: str
    album: str
    year: Optional[int] = None
    status: Optional[str] = None

    def to_entry(self):
        return mu_list.AlbumEntry(self.album, self.year, self.status)

@app.get("/")
async def root():
    return {"message": "/mu Essentials List"}

@app.get("/artist/")
async def get_artist(artist: str = "Radiohead"):
    if artist not in mu_dict:
        raise HTTPException(status_code=404, detail="Artist not found")
    return {"message": mu_dict[artist]}

class ArtistStats(BaseModel):
    artist: str
    done: int
    progr: int
    new: int

@app.get("/stats/", response_model=ArtistStats)
def artist_stats(artist: str = "Radiohead"):
    if artist not in mu_dict:
        raise HTTPException(status_code=404, detail="Artist not found")
    statuses = list(map(lambda e: e.status, mu_dict[artist]))
    done = statuses.count("done")
    progr = statuses.count("progr")
    new = statuses.count("new")
    return {"artist": artist, "done": done, "progr": progr, "new": new}

@app.post("/album/", status_code=201, response_model=ArtistStats)
async def add_album(album: NewAlbumRequest):
    if not album.status:
        album.status = "new"
    if not album.year:
        album.year = 2021
    if album.year > 2021:
        raise HTTPException(status_code=400, detail="Cannot add albums from the future")

    albums = []
    if album.artist in mu_dict:
        albums = mu_dict[album.artist]
    for entry in albums:
        if entry.name == album.album:
            raise HTTPException(status_code=400, detail="Album already exists")
    albums.append(album.to_entry())
    mu_dict[album.artist] = albums
    return artist_stats(album.artist)
